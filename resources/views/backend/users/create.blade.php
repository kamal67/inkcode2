@extends('master_layouts.app')

@section('content_head')
    @include('master_layouts.includes.menu_content_head',['title_page'=>__('title_page.users_management')])
@endsection
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="">
            @include('includes.errors')
        </div>
        <div class="">
            @include('includes.success')
        </div>
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--tabs">
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <form enctype="multipart/form-data" class="form" id="kt_form" method="POST"
                                  action="{{route('users.store')}}">
                                @csrf
                                <div class="kt-form kt-form--label-right">
                                    <div class="kt-form__body">
                                        <div class="kt-section kt-section--first">
                                            <div class="kt-section__body">
                                                <div class="row">
                                                    <label class="col-xl-3"></label>
                                                    <div class="col-lg-7 col-xl-6">
                                                        <h3 class="kt-section__title kt-section__title-sm">{{__('dashboard.users.user_info')}}
                                                            :</h3>
                                                    </div>
                                                    <label class="col-xl-2"></label>
                                                </div>
                                                <div class="form-group row">
                                                    <label
                                                        class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.image')}}</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="kt-avatar kt-avatar--outline  kt-avatar--circle"
                                                             id="kt_user_edit_avatar">
                                                            <div class="kt-avatar__holder"
                                                                 style="background-image: url('{{asset('assets/media/users/default.jpg')}}');"
                                                            ></div>
                                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip"
                                                                   title="" data-original-title="Change image">
                                                                <i class="fa fa-pen"></i>
                                                                <input type="file" name="image"
                                                                       accept=".png, .jpg, .jpeg">
                                                            </label>
                                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip"
                                                                  title="" data-original-title="Cancel image">
																				<i class="fa fa-times"></i>
																			</span>
                                                        </div>
                                                    </div>
                                                    <label class="col-xl-2"></label>
                                                </div>
                                                <div class="form-group row">
                                                    <label
                                                        class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.full_name')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input required
                                                               placeholder="{{__('dashboard.placeholder.name')}}"
                                                               name="name"
                                                               class="form-control" type="text" value="">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                <div>
                                                    <input hidden type="text" value="" name="id">
                                                </div>
                                                <div class="form-group row">
                                                    <label
                                                        class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.user_type')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <select name="user_type"
                                                                class="form-control form-control-solid">
                                                            <option value="customer">{{__('dashboard.users.customer')}}</option>
                                                            <option value="provider">{{__('dashboard.users.provider')}}</option>
                                                        </select>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                <div class="form-group row">
                                                    <label
                                                        class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.phone')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <div class="input-group input-group-solid">
                                                            <div class="input-group-prepend"><span
                                                                    class="input-group-text"><i class="la la-phone"></i></span>
                                                            </div>
                                                            <input required disabled type="text" class="form-control"
                                                                   value=""
                                                                   placeholder="{{__('dashboard.placeholder.phone')}}"
                                                                   aria-describedby="basic-addon1">
                                                        </div>
                                                        <span
                                                            class="form-text text-muted">{{__('custom_messages.general_messages.your_phone_is_private')}}</span>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>

                                                <div class="form-group row">
                                                    <label
                                                        class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.email')}}</label>
                                                    <div class="col-lg-6 col-xl-6 ">
                                                        <div class="input-group  input-group-solid">
                                                            <div class="input-group-prepend"><span
                                                                    class="input-group-text"><i
                                                                        class="la la-at"></i></span></div>
                                                            <input type="email" class="form-control" name="email"
                                                                   value="" placeholder="Email"
                                                                   aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.password')}}</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <input name="password" type="password" class="form-control"
                                                               value=""
                                                               placeholder="New password">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.c_password')}}</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <input name="c_password" type="password" class="form-control"
                                                               value=""
                                                               placeholder="Verify password">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label
                                                        class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.birthday')}}</label>
                                                    <div class="col-lg-6 col-xl-6 ">
                                                        <div class="input-group  input-group-solid">
                                                            <div class="input-group-prepend"><span
                                                                    class="input-group-text"><i
                                                                        class="la la-calendar"></i></span></div>
                                                            <input required type="date" class="form-control"
                                                                   name="birthday"
                                                                   value="" placeholder="birthday"
                                                                   aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-3">{{__('dashboard.account_status.account_status')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <select name="account_status"
                                                                class="form-control form-control-solid">
                                                            <option value="active">{{__('dashboard.account_status.active')}} </option>
                                                            <option value="pending">{{__('dashboard.account_status.pending')}} </option>
                                                        </select>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                <div class="form-group row">
                                                    <label
                                                        class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.talking_lang')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input required
                                                               placeholder="{{__('dashboard.placeholder.talking_lang')}}"
                                                               name="talking_lang"
                                                               class="form-control" type="text"
                                                               value="">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                <div class="form-group row">
                                                    <label
                                                        class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.document_id')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input required
                                                               placeholder="{{__('dashboard.placeholder.document_id')}}"
                                                               name="document_id"
                                                               class="form-control" type="text"
                                                               value="">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                <div class="form-group row">
                                                    <label
                                                        class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.document')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="kt-avatar kt-avatar--outline  kt-avatar--brand"
                                                                 id="kt_user_edit_document">
                                                                <div class="kt-avatar__holder"
                                                                     style=""></div>
                                                                <label class="kt-avatar__upload"
                                                                       data-toggle="kt-tooltip"
                                                                       title="" data-original-title="Change document">
                                                                    <i class="fa fa-pen"></i>
                                                                    <input type="file" name="document"
                                                                           accept=".png, .jpg, .jpeg"
                                                                           value="">
                                                                </label>
                                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip"
                                                                      title="" data-original-title="Cancel document">
																				<i class="fa fa-times"></i>
																			</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                <div class="form-group row">
                                                    <label
                                                        class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.bio')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input required
                                                               placeholder="{{__('dashboard.placeholder.bio')}}"
                                                               name="bio"
                                                               class="form-control" type="text" value="">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>

                                            </div>

                                            <div
                                                class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>
                                            <div class="kt-form__actions">
                                                <div class="row d-flex justify-content-center">
                                                    <div class="col-xl-3"></div>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <a href="{{route('users.index','provider')}}"
                                                           class="btn btn-clean btn-bold">
                                                            <i class="ki ki-long-arrow-back icon-sm"></i>{{__('dashboard.back')}}
                                                        </a>
                                                        <button type="submit"
                                                                class="btn btn-label-brand btn-bold font-weight-bold ">
                                                            <i class="ki ki-check icon-sm"></i>{{__('dashboard.add')}}
                                                        </button>
                                                    </div>

                                                    <label class="col-xl-3"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('wjez/js/user_table.js')}}"></script>
    <script src="{{asset('assets/js/pages/custom/user/edit-user.js')}}"></script>
@endsection

