@extends('master_layouts.app')

@section('content_head')
    @include('master_layouts.includes.menu_content_head',['title_page'=>__('title_page.users_management')])
@endsection
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--tabs">
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <form enctype="multipart/form-data" class="form" id="kt_form" method=""
                                  action="">
                                <div class="kt-form kt-form--label-right">
                                    <div class="kt-form__body">
                                        <div class="kt-section kt-section--first">
                                            <div class="kt-section__body">
                                                <div class="row">
                                                    <label class="col-xl-3"></label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <h3 class="kt-section__title kt-section__title-sm">{{__('dashboard.users.customer_info')}}
                                                            :</h3>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-4"></div>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <div class="kt-avatar kt-avatar--outline  kt-avatar--circle"
                                                             id="kt_user_edit_avatar">
                                                            <div class="kt-avatar__holder"
                                                                 @if(is_null($user->image))
                                                                 style="background-image: url('{{asset('assets/media/users/default.jpg')}}');"
                                                                 @endif
                                                                 @if(!is_null($user->image))
                                                                 style="background-image: url({{$user->image}});"
                                                                @endif
                                                            ></div>
                                                        </div>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.full_name')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input disabled
                                                               placeholder="{{__('dashboard.placeholder.name')}}"
                                                               name="name"
                                                               class="form-control" type="text" value="{{$user->name}}">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                <div>
                                                    <input hidden type="text" value="{{$user->id}}" name="id">
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.user_type')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <select disabled name="user_type"
                                                                class="form-control form-control-solid">
                                                            <option
                                                                @if($user->user_type == 'provider')
                                                                selected
                                                                @endif
                                                                value="provider">{{__('dashboard.users.provider')}}</option>
                                                            <option
                                                                @if($user->user_type == 'customer')
                                                                selected
                                                                @endif
                                                                value="customer">{{__('dashboard.users.customer')}}</option>
                                                            <option
                                                                @if($user->user_type == 'admin')
                                                                selected
                                                                @endif
                                                                value="admin">{{__('dashboard.users.admin')}}</option>
                                                        </select>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.phone')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <div class="input-group input-group-solid">
                                                            <div class="input-group-prepend"><span
                                                                    class="input-group-text"><i class="la la-phone"></i></span>
                                                            </div>
                                                            <input required disabled type="text" class="form-control"
                                                                   value="{{$user->phone}}"
                                                                   placeholder="{{__('dashboard.placeholder.phone')}}"
                                                                   aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.email')}}</label>
                                                    <div class="col-lg-6 col-xl-6 ">
                                                        <div class="input-group  input-group-solid">
                                                            <div class="input-group-prepend"><span
                                                                    class="input-group-text"><i
                                                                        class="la la-at"></i></span></div>
                                                            <input disabled type="email" class="form-control" name="email"
                                                                   value="{{$user->email}}" placeholder="Email"
                                                                   aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label class="col-2">{{__('dashboard.account_status.account_status')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <select disabled name="account_status"
                                                                class="form-control form-control-solid">
                                                            <option
                                                                @if($user->account_status == 'active')
                                                                selected
                                                                @endif
                                                                value="active">{{__('dashboard.account_status.active')}}
                                                            </option>
                                                            <option
                                                                @if($user->account_status == 'pending')
                                                                selected
                                                                @endif
                                                                value="pending">{{__('dashboard.account_status.pending')}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                            </div>
                                            <div
                                                class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>
                                            <div class="kt-form__actions">
                                                <div class="row d-flex justify-content-center">
                                                    <div class="col-xl-3"></div>
                                                    <div class="col-lg-6 col-xl-6">

                                                        <a href="{{route('users.index','customer')}}"
                                                           class="btn btn-clean btn-bold">
                                                            <i class="ki ki-long-arrow-back icon-sm"></i>{{__('dashboard.back')}}
                                                        </a>

                                                        <a href="{{route('users.edit',$user->id)}}"
                                                           class="btn btn-label-brand btn-bold font-weight-bold ">
                                                            <i class="ki ki-check icon-sm"></i>{{__('dashboard.edit')}}
                                                        </a>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('wjez/js/user_table.js')}}"></script>
    <script src="{{asset('assets/js/pages/custom/user/edit-user.js')}}"></script>
@endsection

