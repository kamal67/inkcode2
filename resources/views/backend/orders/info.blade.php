@extends('master_layouts.app')

@section('content_head')
    @include('master_layouts.includes.menu_content_head',['title_page'=>__('dashboard.orders.management')])
@endsection
@section('content')

    <div class="">
        @include('includes.errors')
    </div>
    <div class="">
        @include('includes.success')
    </div>

    <div class="col-12">
        <div class="card-header" style="">
            <div class="card-toolbar row">
                <a href="{{route('dashboard')}}" class="btn btn-light-primary font-weight-bolder mr-2">
                    <i class="flaticon2-back icon-sm"></i>{{__('dashboard.back')}}</a>

            </div>
        </div>

        <div class="kt-portlet">

            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div class="kt-portlet__body kt-portlet__body--fit ">
                    <div class="row  mt-2">
                        <div class="col-12" style="color: black ">
                          <h6>  <b> Order Number : {{ $order->order_number }} </b><br></h6></h6>
                          <h6>  <b> Owner Name : {{ $order->owner->name }} </b><br></h6>
                          <h6>    <b> Cost  : {{ $order->final_total }} </b><br></h6>
                          <h6>   <b> Created at : {{ $order->updated_at }} </b><br></h6>
                        </div>
                    </div>
                    <div class="  row">
                        <div class="col-md-12 col-sm-12 mt-5 ">
                            <div id="kt_subheader_search">
                                <form id="kt_subheader_search_form">
                                    <div class="kt-input-icon kt-input-icon--left kt-subheader__search">
                                        <input type="text" class="form-control" placeholder="Search..."
                                               id="generalSearch">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left">
													<span>
														<svg xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1"
                                                             class="kt-svg-icon">
															<g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
																<rect x="0" y="0" width="24" height="24"></rect>
																<path
                                                                    d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                                                    fill="#000000" fill-rule="nonzero"
                                                                    opacity="0.3"></path>
																<path
                                                                    d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                                                    fill="#000000" fill-rule="nonzero"></path>
															</g>
														</svg>

													</span>
												</span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                        <div class="kt-portlet__body kt-portlet__body--fit">

                            <table class="kt-datatable table-hover" id="kt-datatable" width="100%">
                                <thead>
                                <tr>

                                    <th>{{__('dashboard.products.image')}}</th>
                                    <th>{{__('dashboard.name')}}</th>

                                    <th>{{__('dashboard.products.brand_id')}}</th>
                                    <th>{{__('dashboard.products.category_id')}}</th>
                                    <th>{{__('dashboard.controls')}}</th>
                                </tr>
                                </thead>
                                <tbody>

                                    @foreach($order->items as $datum)
                                    <tr>
                                        <td style="text-align: center !important;">

                                            @if(!is_null($datum->product->media_path))
                                                <img src="{{asset($datum->product->media_path)}}" alt="image"  alt="image" style=" height: 135px !important;width: 135px !important;"
                                                     data-toggle="popover-hover"
                                                     data-img="{{asset($datum->product->media_path)}}">
                                            @else
                                                <img src="{{asset('assets/media/company-logos/logo.png')}}" alt="image" style=" height: 135px !important;width: 135px !important;"
                                                     data-toggle="popover-hover"
                                                     data-img="{{asset('assets/media/company-logos/logo.png')}}">
                                            @endif

                                        </td>
                                        <td style="text-align: center !important;">
                                            <div class="kt-user-card-v2 overflow-hidden">
                                                <div class="kt-user-card-v2__details">
                                                    <a class="kt-user-card-v2__name" href="#">
                                                        @if(\Illuminate\Support\Facades\Session::get('locale') =='ar')
                                                            {{$datum->product->name_ar}}
                                                        @endif
                                                        @if(\Illuminate\Support\Facades\Session::get('locale') =='en')
                                                            {{$datum->product->name_en}}
                                                        @endif
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="text-align: center !important;">
                                            <div class="kt-user-card-v2 overflow-hidden">
                                                <div class="kt-user-card-v2__details">
                                                    <a class="kt-user-card-v2__name" href="#">
                                                        @if(\Illuminate\Support\Facades\Session::get('locale') =='ar')
                                                            {{$datum->product->brand->name_ar??''}}
                                                        @endif
                                                        @if(\Illuminate\Support\Facades\Session::get('locale') =='en')
                                                            {{$datum->product->brand->name_en??''}}
                                                        @endif
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="text-align: center !important;">
                                            <div class="kt-user-card-v2 overflow-hidden">
                                                <div class="kt-user-card-v2__details">
                                                    <a class="kt-user-card-v2__name" href="#">
                                                        @if(\Illuminate\Support\Facades\Session::get('locale') =='ar')
                                                            {{$datum->product->category->name_ar ??''}}
                                                        @endif
                                                        @if(\Illuminate\Support\Facades\Session::get('locale') =='en')
                                                            {{$datum->product->category->name_en ??''}}
                                                        @endif
                                                    </a>
                                                </div>
                                            </div>
                                        </td>



                                        {{--                                        controls--}}
                                        <td style="text-align: center !important;">
                                            <div class="dropdown ">
                                                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                                   data-toggle="dropdown">
                                                    <i class="flaticon-more-1"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="kt-nav">

                                                        <li class="kt-nav__item">
                                                            <a id="delete_user" class=" kt-nav__link"
                                                               style="cursor: pointer "
                                                               href="{{route('orders.items.delete',$datum->id)}}"
                                                               title="Delete">
                                                                <i class=" kt-nav__link-icon flaticon-delete"></i>
                                                                <span
                                                                    class="kt-nav__link-text">{{__('dashboard.delete')}}</span>
                                                            </a>
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </td>

                                    </tr>

                                    @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

            @endsection
            @section('js')
                <script src="{{asset('wjez/js/user_table.js')}}"></script>

@endsection

