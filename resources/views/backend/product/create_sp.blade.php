
@extends('master_layouts.app')
@section('style')

@endsection
@section('content_head')
    @include('master_layouts.includes.menu_content_head',['title_page'=>__('title_page.gift_management')])
@endsection
@section('content')
    <div class="">
        @include('includes.errors')
    </div>
    <div class="">
        @include('includes.success')
    </div>
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">


            <div class="kt-portlet kt-portlet--tabs">
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <form enctype="multipart/form-data" class="form" id="kt_form" method="POST"
                                  action="{{route('gift.soterSpecialProduct')}}">
                                @csrf
                                <div class="kt-form kt-form--label-right">
                                    <div class="kt-form__body">
                                        <div class="kt-section kt-section--first">
                                            <div class="kt-section__body">

                                                <div class="row">
                                                    <label class="col-xl-3"></label>
                                                    <div class="col-lg-7 col-xl-6">
                                                        <h3 class="kt-section__title kt-section__title-sm">{{__('dashboard.gifts.special_gift')}}
                                                            :</h3>
                                                    </div>
                                                    <label class="col-xl-2"></label>
                                                </div>
                                                {{--image--}}
                                                <div class="form-group row">
                                                    <label class="col-1"></label>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.gifts.image_sp')}}</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="kt-avatar kt-avatar--outline"  style="width: 250px !important;height: 250px !important;"
                                                             id="kt_user_edit_avatar">
                                                            <div class="kt-avatar__holder"
                                                                 style="background-image: url('{{asset($data->media_path ??'assets/media/company-logos/logo.png')}}');background-size: contain; ;background-size:cover;width: 250px !important;height: 250px !important;"
                                                            ></div>
                                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip"
                                                                   title="" data-original-title="Change image">
                                                                <i class="fa fa-pen"></i>
                                                                <input type="file" name="image"
                                                                       accept=".png, .jpg, .jpeg">
                                                            </label>
                                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip"
                                                                  title="" data-original-title="Cancel image">
																				<i class="fa fa-times"></i>
																			</span>
                                                        </div>
                                                    </div>
                                                    <label class="col-xl-2"></label>
                                                </div>



                                                {{--English Name--}}
                                                <div class="form-group row">
                                                    <label class="col-1"></label>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.gifts.title_en')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input required
                                                               placeholder="{{__('dashboard.gifts.title_en')}}"
                                                               name="title_en"
                                                               class="form-control" type="text" value="{{ $data->title_ar ??'' }}">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--Arabic Name--}}
                                                <div class="form-group row">
                                                    <label class="col-1"></label>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.gifts.title_ar')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input required
                                                               placeholder="{{__('dashboard.gifts.title_ar')}}"
                                                               name="title_ar"
                                                               class="form-control" type="text" value="{{ $data->title_ar ??'' }}">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>

                                                {{--English text--}}
                                                <div class="form-group row">
                                                    <label class="col-1"></label>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.gifts.text_en')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <textarea required

                                                               name="text_en"
                                                               class="form-control" type="text" value="">{{ $data->text_en ??'' }}</textarea>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--Arabic Name--}}
                                                <div class="form-group row">
                                                    <label class="col-1"></label>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.gifts.text_ar')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <textarea required

                                                               name="text_ar"
                                                               class="form-control" type="text" value="">{{ $data->text_ar ??'' }}</textarea>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>




                                                {{--parent category--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.gifts.gift')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <select name="gift_id"
                                                                class="form-control form-control-solid customSelect" required>
                                                            <option selected disabled hidden
                                                                    value="{{null}}">{{__('dashboard.gifts.gift')}}</option>

                                                            @foreach($fks as $cate)
                                                                <option value="{{$cate->id}}" {{

                                                                    ($data->gift_id??0 == $cate->id) ? 'selected':''
                                                                }}
                                                                >
                                                                    @if(app()->getLocale() =='ar')
                                                                        {{$cate->name_ar}}
                                                                    @endif
                                                                    @if(app()->getLocale() =='en')
                                                                        {{$cate->name_en}}
                                                                    @endif
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>


                                            </div>




                                            </div>








                                            </div>

                                            <div
                                                class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>
                                            <div class="kt-form__actions">
                                                <div class="row d-flex justify-content-center">
                                                    <div class="col-xl-3"></div>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <a href="javascript:history.back()"
                                                           class="btn btn-clean btn-bold">
                                                            <i class="ki ki-long-arrow-back icon-sm"></i>{{__('dashboard.back')}}
                                                        </a>
                                                        <button type="submit"
                                                                class="btn btn-label-brand btn-bold font-weight-bold ">
                                                            <i class="ki ki-check icon-sm"></i>{{__('dashboard.gifts.create')}}
                                                        </button>
                                                    </div>


                                                    <label class="col-xl-3"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
      
    </div>
@endsection
@section('js')



     <script type="text/javascript">
                    $(document).ready(function(){
                    var maxField = 50; //Input fields increment limitation
                    var addButton = $('.add_button_color'); //Add button selector
                    var wrapper = $('.repeat_color'); //Input field wrapper
                    var fieldHTML = '<div class="col-lg-2 col-xl-2"><div><input type="color" class="form-control" name="color[]" value=""/> <input required name="media_path_wrap[]" class="form-control" type="file" value="">  <a href="javascript:void(0);" class="remove_button btn btn-primary btn-sm"><span class="fa fa-minus" ></span></a></div></div>'; //New input field html

                    var x = 1; //Initial field counter is 1

                    //Once add button is clicked
                    $(addButton).click(function(){
                    //Check maximum number of input fields
                    if(x < maxField){
                    x++; //Increment field counter
                    $(wrapper).append(fieldHTML); //Add field html
                }
                });

                    //Once remove button is clicked
                    $(wrapper).on('click', '.remove_button', function(e){
                    e.preventDefault();
                    $(this).parent('div').remove(); //Remove field html
                    x--; //Decrement field counter
                });
                });
            </script>
     <script type="text/javascript">
                    $(document).ready(function(){
                    var maxField = 50; //Input fields increment limitation
                    var addButton = $('.add_button_size_price'); //Add button selector
                    var wrapper = $('.repeat_size_and_price'); //Input field wrapper
                    var fieldHTML = '<div class="col-lg-6 col-xl-6"> <input required name="size[]" class="form-control" type="text" value="" placeholder="{{__('dashboard.size')}}"><input required name="price[]" class="form-control" type="number" value="" min="0" max="99999999" placeholder="{{__('dashboard.price')}}"><a href="javascript:void(0);" class="remove_button btn btn-primary btn-sm" title="Add field"><span class="fa fa-minus" ></span></a></div>';
                    var x = 1; //Initial field counter is 1

                    //Once add button is clicked
                    $(addButton).click(function(){
                    //Check maximum number of input fields
                    if(x < maxField){
                    x++; //Increment field counter
                    $(wrapper).append(fieldHTML); //Add field html
                }
                });

                    //Once remove button is clicked
                    $(wrapper).on('click', '.remove_button', function(e){
                    e.preventDefault();
                    $(this).parent('div').remove(); //Remove field html
                    x--; //Decrement field counter
                });
                });
            </script>
    <script>



        $(function(){

            $("#repeater").createRepeater();


        });
    </script>
    <script src="{{asset('wjez/js/user_table.js')}}"></script>
    <script src="{{asset('assets/js/pages/custom/user/edit-user.js')}}"></script>

@endsection

