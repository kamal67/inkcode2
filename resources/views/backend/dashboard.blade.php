@extends('master_layouts.app')

@section('content_head')
    @include('master_layouts.includes.menu_content_head',['title_page'=>__('title_page.home_page')])
@endsection
@section('content')
    <div class="">
        @include('includes.errors')
    </div>
    <div class="">
        @include('includes.success')
    </div>


@endsection
@section('js')
    <script src="{{asset('wjez/js/user_table.js')}}"></script>
    @if(session()->has('showToastr'))
        <script !src="">$(document).ready(function () {
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "icon": null,
                    "hideMethod": "fadeOut"
                };

                toastr.warning("You are logged in!");





            });
        </script>
        @php
        session()->remove('showToastr')
        @endphp
    @endif
@endsection











