@if (session('status'))
    <div class="row mt-2">

        <div class="col-sm-12">
            <div class="alert alert-warning fade show text-white" role="alert">
                <div class="alert-icon text-white"><i class="flaticon2-check-mark " style="color: #fff !important;"></i></div>
                <div class="alert-text text-white"><span>{{session('status')}}</span></div>
                <div class="alert-close text-white">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="la la-close"></i></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endif
