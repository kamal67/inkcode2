<!DOCTYPE html>

@if(   \Illuminate\Support\Facades\Session::get('locale') =='ar')
    <html direction="rtl" dir="rtl" style="direction: rtl">
    @else
        <html lang="en">
        @endif

        <head>
            <meta charset="utf-8"/>
            <title>{{ config('app.name') }}</title>
            <meta name="description" content="Updates and statistics">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="shortcut icon" href="{{asset('assets/media/company-logos/logo.png')}}"/>

            <link rel="stylesheet"
                  href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">




            <link href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet"
                  type="text/css"/>



            <link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css"/>

            @if(\Illuminate\Support\Facades\Session::get('locale') =='ar')
                <link href="{{asset('assets/css/style.bundle.rtl.css')}}" rel="stylesheet" type="text/css"/>
            @else
                <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css"/>
            @endif





            <link href="{{asset('assets/css/skins/header/base/light.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{asset('assets/css/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{asset('assets/css/skins/brand/light.css')}}" rel="stylesheet" type="text/css"/>
            <link href="{{asset('assets/css/skins/aside/light.css')}}" rel="stylesheet" type="text/css"/>





            <style>
                @font-face {
                    font-family: "cadue";
                    src: url({{ asset("assets/fonts/brush script mt kursiv.ttf")}});
                    src: url({{asset( "assets/fonts/BRUSHSCI.ttf")}});
                }

                .cadue {
                    font-family: cadue;
                }


            </style>
            @yield('style')



        </head>




        <body
            class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">



        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__logo">
                <a href="index.html">
                    <img alt="Logo" src="{{asset('')}}"/>
                </a>
            </div>
            <div class="kt-header-mobile__toolbar">
                <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler">
                    <span></span></button>
                <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
                <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                        class="flaticon-more"></i></button>
            </div>
        </div>


        <div class="kt-grid kt-grid--hor kt-grid--root">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">


            @include('master_layouts.includes.sidebar')
                 <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                @include('master_layouts.includes.top_header')

                    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">


                    @yield('content_head')




                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            @yield('content')

                        </div>


                    </div>


                </div>
            </div>

        @include('master_layouts.includes.footer')

        </div>




        <div id="kt_scrolltop" class="kt-scrolltop">
            <i class="fa fa-arrow-up"></i>
        </div>



        <script>
            function switchLang(formID) {
                document.getElementById(formID).submit();
            }
        </script>




        <script src="{{asset('assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/js/myScript.js')}}" type="text/javascript"></script>


   <script>
            $(document).ready(function(){
                $('.customSelect').select2();

                // popovers initialization - on hover
                $('[data-toggle="popover-hover"]').popover({
                    html: true,
                    trigger: 'hover',
                    placement: 'bottom',
                    content: function () { return '<img src="' + $(this).data('img') + '" />'; }
                });

                // popovers initialization - on click
                $('[data-toggle="popover-hover"]').popover({

                    html: true,
                    trigger: 'click',
                    placement: 'bottom',
                    content: function () { return '<img src="' + $(this).data('img') + '" />'; }
                });




                         $('.sle').select2();

                        $('.sl_mt').select2({
                            placeholder: 'Select',
                            multiple: true,
                            allowClear: true
                        });

                          $('.justNumber').keypress(function(evt) {
                            if (evt.which < 48 || evt.which > 57) {
                                evt.preventDefault();
                            }
                        });


            });

        </script>



        <script src="{{asset('assets/js/pages/dashboard.js')}}" type="text/javascript"></script>
        {{-- <script src="{{asset('repeater.js')}}"></script> --}}
        @yield('js')




        </body>


        </html>
