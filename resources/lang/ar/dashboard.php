<?php

return [
    'arabic' => 'عربي',
    'english' => 'انجليزي',
    'all' => 'الكل',
    'remember_me' => 'تذكرني',
    'email' => 'البريد الالكتروني',
    'password' => 'كلمة المرور',
    'c_password' => 'تأكيد كلمة المرور',
    'current_password' => 'كلمة المرور الحالية',
    'new_password' => 'كلمة المرور الجديدة',
    'change_password' => 'تغيير كلمة المرور',
    'forget_password' => 'نسيت كلمة المرور ؟ ',
    'sign_in' => 'تسجيل دخول',
    'language' => 'اللغة',
    'brand_name' => 'الماركة',
    'text_en' => 'نص انكليزي',
    'text_ar' => 'نص عربي',
    'cost_songe' => 'كلفة الاغنية',
    'save_changes' => 'كلفة الاغنية',

    'price' => 'السعر',
    'count_color' => 'عدد الالوان',
    'count_sizes' => 'عدد الاحجام',
    'count' => 'العدد ',
    'view_items' => 'عرض العناصر',
    'full_name' => 'الاسم الكامل',
    'name' => 'الاسم',
    'en_name' => 'الاسم بالانجليزي',
    'ar_name' => 'الاسم بالعربي',
    'type' => 'النوع',
    'phone' => 'الهاتف',
    'creation_date' => 'تاريخ الانشاء',
    'controls' => 'خيارات',
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'user_type' => 'نوع المستخدم',
    'user_status' => 'حالة المستخدم',
    'save_change' => 'حفظ التغيرات',
    'back' => 'رجوع',
    'cancel' => 'الغاء',
    'image' => 'صورة',
    'profile' => 'الملف الشخصي',
    'talking_lang' => 'اللغة التي يتحدث بها',
    'document_id' => 'رقم الوثيقة',
    'document' => ' الوثيقة',
    'birthday' => ' تاريخ الميلاد',
    'bio' => ' نبذة قصيرة',
    'city' => 'المدينة',
    'more_info' => 'المزيد من الملومات',
    'what_do_you_provide' => 'ما الذي تقدمه',
    'choose_service' => 'اختر خدمة',
    'about_app' => 'حول التطبيق',
    'privacy_policy' => 'سياسة الخصوصية',
    'placeholder' => [
        'password' => 'ادخل كلمة المرور',
        'c_password' => 'اعد كتابة كلمة المرور',
        'talking_lang' => 'ادخل اللغة التي يتحدث بها',
        'name' => 'ادخل اسمك',
        'phone' => 'ادخل رقم الهاتف',
        'document_id' => 'ادخل رقم الوثيقة',
        'bio' => ' اكتب نبذة قصيرة',
    ],
    'account_status' => [
        'account_status' => 'حالة الحساب',
        'inactive' => 'غير مفعل',
        'deleted' => 'محذوف',
        'pending' => 'معلق',
        'active' => 'مفعل',
    ],
    'category' => [
        'image' => 'صورة القسم',
        'choose_parent' => 'اختر القسم الأساسي',
        'category_info' => 'معلومات القسم',
        'add' => 'اضافة قسم',
        'is_main' => 'رئيسية',
        'main_category' => 'الفئة الاساسية',
        'yes' => 'نعم',
        'no' => 'لا',
        'placeholder' => [
            'name' => 'ادخل اسم القسم باللغة الانلجليزية',
            'name_ar' => 'ادخل اسم القسم باللغة العربية',
        ],
    ],
    'users' => [
        'add' => 'اضافة مستخدم',
        'user_info' => 'معلومات المستخدم',
        'provider_info' => 'معلومات مزود الخدمة',
        'customer_info' => 'معلومات طالب الخدمة',
        'admin_info' => 'معلومات المدير',
        'admin' => ' المدير',
        'provider' => 'مزود خدمة',
        'customer' => 'زبون',
        'user' => 'المستخدم',
    ],
    'service' => [
        'service_request_info' => 'معلومات طلب الخدمة',
        'service' => ' الخدمة',
        'problem_details' => 'تفاصيل المشكلة',
        'status' => 'حالة الطلب',
        'date_request' => 'تاريخ الطلب',
        'cost' => 'الكلفة',
    ],
    'times' => [
        'today' => 'اليوم',
        'this_week' => 'هذا الأسبوع',
        'this_month' => 'هذا الشهر',
        'this_year' => 'هذه السنة',
        ],

    'ads' => [
        'title' => 'العنوان',
        'ads_type' => 'نمط الاعلان',
        'description' => 'الوصف',
        'category_id' => 'الفئة',
        'media_type' => 'نوع الاعلان',
        'media_path' => 'الاعلان',
        'start_date' => 'تاريخ البدء',
        'end_date' => 'تاريخ النهاية',
        'link' => 'الرابط',
        'global' => 'عام',
        'specific' => 'مخصص',
    ],
    'brand' => [
        'image' => 'صورة الماركة',

        'brand_info' => 'معلومات الماركة',
        'add' => 'اضافة ماركة',
        'edit' => 'اضافة ماركة',

        'placeholder' => [
            'name' => 'ادخل اسم الماركة باللغة الانلجليزية',
            'name_ar' => 'ادخل اسم الماركة باللغة العربية',
        ],
    ],

    'slider' => [
        'image' => 'صورة السلايد',
        'info' => 'معلومات السلايد',
        'add' => 'اضافة سلايد',
        'edit' => ' تعديل السلايد',
    ],

    'occasions' => [
        'image_large' => 'الصورة الكبيرة',
        'image_icon' => 'الايقونة',
        'info' => 'معلومات المناسبة',
        'add' => 'اضافة مناسبة',
        'edit' => 'تعديل المناسبة',
        'placeholder' => [
            'name' => 'ادخل اسم المناسبة باللغة الانلجليزية',
            'name_ar' => 'ادخل اسم المناسبة باللغة العربية',
        ],
    ],
    'relations' => [
        'image_large' => 'الصورة الكبيرة',
        'image_icon' => 'الايقونة',
        'info' => 'معلومات العلاقة',
        'add' => 'اضافة علاقة',
        'edit' => 'تعديل العلاقة',
        'placeholder' => [
            'name' => 'ادخل اسم العلاقة باللغة الانلجليزية',
            'name_ar' => 'ادخل اسم العلاقة باللغة العربية',
        ],
    ],
    'wraps' => [
        'image' => 'الصورة الكبيرة',
        'wraps_group' => 'غروب التغليف',
        'info_group' => 'معلومات غروب تغليف',
        'add_group' => 'اضافة غروب تغليف',
        'info' => 'معلومات  التغليف',
        'add' => 'اضافة تغليف',
        'edit' => 'تعديل تغليف',
        'placeholder' => [
            'name' => 'ادخل اسم التغليف باللغة الانلجليزية',
            'name_ar' => 'ادخل اسم التغليف باللغة العربية',
        ],
        'placeholder_group' => [
            'name' => 'ادخل اسم غروب التغليف باللغة الانلجليزية',
            'name_ar' => 'ادخل اسم غروب التغليف باللغة العربية',
        ],
    ],
    'belong_to_group' => 'غروب التغليف',
    'color_and_image' => 'لون وصورة',
    'size_and_price' => 'حجم وسعر',
    'size_and_price' => 'حجم وسعر',
    'size' => 'الحجم',
    'products' => [
        'image' => 'صورة  ',
        'info' => 'معلومات  ',
        'add' => 'إضافة  ',
        'yes' => 'نعم',
        'no' => 'لا',
        'placeholder' => [
            'name' => 'ادخل اسم   باللغة الانلجليزية',
            'name_ar' => 'ادخل اسم   باللغة العربية',
        ],

        'main_price' => 'السعر الاساسي',
        'brand_id' => 'الماركة',
        'category_id' => 'الفئة',
        'sale_price' => 'سعر المبيع',
        'description' => 'الوصف',
        'likes' => 'اعجابات',
        'for_gender' => 'تعود إلى',
        'all_relations' => 'كل العلاقات',
        'relations' => ' العلاقات',
        'sold_operator' => 'معامل الحسم',
        'sold_price' => 'سعر بعد الحسم',
        'min_age' => 'العمر الاصغري',
        'max_age' => 'العمر الاعظمي',

        'title_en' => 'عنوان انكليزي',
        'title_ar' => 'عنوان عربي',
        'text_en' => 'نص انكليزي',
        'text_ar' => 'نص عربي',
        'image_sp' => ' صورة  خاصة',
        'gift' => 'الهدية ',
        'special_gift' => 'هدية خاصة',
        'create' => 'حفظ',
     ],



     'orders' => [
        "management"=>"إدارة الطلبات",
        "all"=>"كل الطلبات",
        "create"=>"انشاء طلب",
        "edit"=>"تعديل طلب",
        "delete"=>"حذف طلب",
        "client_name"=>"اسم الزبون",
        "created_at"=>"تاريخ الانشاء",
        "total"=>"الاجمالي",
        "status"=>"الحالة",
        "number"=>"رقم الطلب",
        "generate_excel"=>" توليد ملف اكسل  ",
        "products"=>"المنتجات",

    ],
];
