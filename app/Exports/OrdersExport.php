<?php
/**
 * Created by PhpStorm.
 * User: Byte7
 * Date: 1/13/2020
 * Time: 10:03 AM.
 */

namespace App\Exports;

use App\Models\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class OrdersExport implements FromCollection, WithHeadings, WithMapping, WithEvents, WithColumnFormatting
{
    public function collection()
    {
        return Order::where('status', 'done')->get()->load('owner');
    }

    public function headings(): array
    {
        return [
            '#',
            'Order number',
            'Clinet Name',
            'Clinet email',
            'Clinet Phone',
            'Total Cost',
            'Creation Date',


        ];
    }

    public function map($row): array
    {
        return [
            $row->id,
            $row->order_number,
            $row->owner->name,
            $row->owner->email,
            $row->owner->country_code.'-'. $row->owner->phone_number,
            $row->total_cost,
            $row->updated_at,

        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_NUMBER,
            'D' => NumberFormat::FORMAT_NUMBER,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(15);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(35);
            },
        ];
    }
}
