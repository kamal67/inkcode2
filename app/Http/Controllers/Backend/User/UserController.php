<?php

namespace App\Http\Controllers\Backend\User;

use App\Http\Controllers\BaseController;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_type)
    {
        try {
            $us = User::query()->where('user_type', $user_type)->where('is_deleted', 0)->get();

            return view('backend.users.index', compact('us', 'user_type'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * @param $user_type
     *
     * @return Application|Factory|View
     */
    public function create($user_type)
    {
        return view('backend.users.'.$user_type.'_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $rules = [
                    'name' => 'required|string|max:191',
                    'email' => 'required|email|unique:users',
                    'password' => 'required|min:6',
                    'c_password' => 'required|same:password|min:6',
                ];
            $res_validate = $this->checkRequest($request, $rules);
            if (!$res_validate['status']) {
                return redirect()->back()->withErrors($res_validate['messages']);
            }

            $data = $request->only([
                    'email',
                    'name',
                    'password',

                    'account_status',
                ]);

            $data['password'] = Hash::make($data['password']);
            $data['user_type'] = $request->user_type;
            $data['phone_number'] = mt_rand(10000000, 99999999);

            $data['country_code'] = '+963';
            $user = User::query()->create($data);
            $user->assignRole('admin');

            $user_type = $request->user_type;
            $us = User::query()->where('user_type', $user_type)->where('is_deleted', 0)->get();

            return redirect()->back()->withStatus(__('custom_messages.general_messages.crud.created'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = \App\Models\User\User::query()->find($id);
            if (is_null($user)) {
                return redirect()->back()->withErrors(__('custom_messages.general_messages.user_not_found'));
            }
            if ($user->user_type === 'customer') {
                return view('backend.users.customer_info', compact('user'));
            }
            if ($user->user_type === 'provider') {
                return view('backend.users.provider_info', compact('user'));
            }
            if ($user->user_type === 'admin') {
                return view('backend.users.admin_info', compact('user'));
            }
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $user = \App\Models\User\User::query()->find($id);
            if (is_null($user)) {
                return redirect()->back()->withErrors(__('custom_messages.general_messages.user_not_found'));
            }
            if ($user->user_type === 'customer') {
                return view('backend.users.customer_edit', compact('user'));
            }
            if ($user->user_type === 'provider') {
                return view('backend.users.provider_edit', compact('user'));
            }
            //if ($user->user_type === 'admin') return view('backend.users.admin_edit', compact('user'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user_id = $request->id;
            $user = User::query()->find($user_id);
            if ($user->user_type == 'provider') {
                $rules = [
                    'name' => 'required|string|max:191',
                    'birthday' => 'required',
                    'talking_lang' => 'required',
                    'document_id' => 'required',
                ];

                if ($request->email != null) {
                    $rules['email'] = 'email';
                }

                $res_validate = $this->checkRequest($request, $rules);
                if (!$res_validate['status']) {
                    return redirect()->back()->withErrors($res_validate['messages']);
                }
                $user = User::query()->find($user_id);
                $user->name = $request->name;
                $user->birthday = $request->birthday;
                $user->talking_lang = $request->talking_lang;
                $user->bio = $request->bio;
                $user->account_status = $request->account_status;
                $user->document_id = $request->document_id;
                if ($request->email != null) {
                    $user->email = $request->email;
                }
                if ($request->city != null) {
                    $user->city = $request->city;
                }
                if ($request->image != null) {
                    $user->image = $this->uploadPersonalPhoto($request);
                }
                if ($request->document != null) {
                    $user->document = $this->uploadDocumentPhoto($request);
                }
                $res = $user->save();

                return redirect(route('users.index', 'provider'))->withStatus(__('custom_messages.general_messages.crud.updated'));
            }
            if ($user->user_type == 'customer') {
                $rules = [
                    'name' => 'required|string|max:191',
                ];
                if ($request->email != null) {
                    $rules['email'] = 'email';
                }
                $res_validate = $this->checkRequest($request, $rules);
                if (!$res_validate['status']) {
                    return redirect()->back()->withErrors($res_validate['messages']);
                }
                $user = User::query()->find($user_id);
                $user->name = $request->name;
                $user->account_status = $request->account_status;
                if ($request->email != null) {
                    $user->email = $request->email;
                }
                if ($request->image != null) {
                    $user->image = $this->uploadPersonalPhoto($request);
                }

                $res = $user->save();

                return redirect(route('users.index', 'customer'))->withStatus(__('custom_messages.general_messages.crud.updated'));
            }
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::query()->find($id);
            $user->is_deleted = 1;
            $user->save();

            return  redirect()->back()->withStatus(__('custom_messages.general_messages.crud.deleted'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    public function changePassword(Request $request, $id)
    {
        try {
            $rules = [
                'old_password' => 'required|min:6',
                'password' => 'required|min:6',
                'c_password' => 'required|same:password|min:6',
            ];
            $res_validate = $this->checkRequest($request, $rules);
            if (!$res_validate['status']) {
                return redirect()->back()->withErrors($res_validate['messages']);
            }
            $user = User::query()->find($id);
            if (Hash::check($request->old_password, $user->password)) {
                $user->password = Hash::make($request->password);
                $res = $user->save();
                if ($user->user_type == 'provider') {
                    return redirect(route('users.index', 'provider'))->withStatus(__('custom_messages.general_messages.reset_password'));
                }
                if ($user->user_type == 'customer') {
                    return redirect(route('users.index', 'customer'))->withStatus(__('custom_messages.general_messages.reset_password'));
                }
                //if ($user->user_type=='admin')
                //return redirect(route('users.index','admin'))->withStatus(__('custom_messages.general_messages.reset_password'));
            } else {
                return redirect()->back()->withErrors(__('custom_messages.general_messages.old_password_incorrect'));
            }
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }
}
