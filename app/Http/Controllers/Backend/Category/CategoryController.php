<?php

namespace App\Http\Controllers\Backend\Category;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $cat = Category::query()->where('is_deleted', 0)->get();

            return view('backend.categories.index', compact('cat'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('backend.categories.create');
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $rules = [
                'name_en' => 'required|string|max:191',
                'name_ar' => 'required|string|max:191',
            ];
            $res_validate = $this->checkRequest($request, $rules);
            if (!$res_validate['status']) return redirect()->back()->withErrors($res_validate['messages']);
            $data = $request->only([
                'name_en',
                'name_ar',
            ]);
            if (!is_null($request->image)) {
                $data['media_path'] = $this->uploadCategoryPhoto($request);
            }

            $category = Category::query()->create($data);

            return  redirect()->route('categories.index')

                ->withStatus(ucfirst(__('custom_messages.general_messages.crud.created')));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try {
            $cat = Category::query()->find($id);
            return view('backend.categories.edit', compact('cat'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $cat_id = $id;
            $category = Category::query()->find($cat_id);
            $rules = [
                'name_en' => 'required|string|max:191',
                'name_ar' => 'required|string|max:191',
            ];
            $res_validate = $this->checkRequest($request, $rules);
            if (!$res_validate['status']) return redirect()->back()->withErrors($res_validate['messages']);
            $category->name_en = $request->name_en;
            $category->name_ar = $request->name_ar;
            if (!is_null($request->image)) {
                $category->media_path = $this->uploadCategoryPhoto($request);
            }

            $category->save();


            return redirect(route('categories.index'))
                ->withStatus(__('custom_messages.general_messages.crud.updated'));

        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $cat = Category::query()->find($id);
            $cat->update([
                'is_deleted'=>1
            ]);
            return  redirect()->back()->withStatus(__('custom_messages.general_messages.crud.deleted'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }
}
