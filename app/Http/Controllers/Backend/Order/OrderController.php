<?php

namespace App\Http\Controllers\Backend\Order;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItems;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends BaseController
{
    public function index()
    {
        try {
            $data = Order::query()->where('status', "done")->get();
            return view('backend.orders.index', compact('data'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $users= User::where('is_deleted',0)->where('user_type','client')->get();
            $products = Product::where('is_deleted',0)->get();
            return view('backend.orders.create',compact('users','products'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $rules = [
                'user_id' => 'required',
                'products' => 'required',
            ];
            $res_validate = $this->checkRequest($request, $rules);
            if (!$res_validate['status']) return redirect()->back()->withErrors($res_validate['messages']);
            $data = $request->only([
                'user_id',
                'products',
            ]);
            $order = Order::getUserOrder($data['user_id']);
            $order->update(['status'=>'done']);
            $orderItems = OrderItems::addMultibleToOrder($data,$order);

            return  redirect()->back()

                ->withStatus(ucfirst(__('custom_messages.general_messages.crud.created')));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try {
            $data = Order::query()->find($id);
            return view('backend.orders.edit', compact('data'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $order = Order::query()->find($id);
            if(is_null($order))  return  redirect()->back()->withErrors(__("Deleted Faild."));

            DB::beginTransaction();

            OrderItems::where('order_id',$order->id)->delete();
            $order->delete();
            DB::commit();
            return  redirect()->back()->withStatus(__('custom_messages.general_messages.crud.deleted'));
        } catch (\Exception $ex) {
            DB::rollBack();
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }


    public function generateExcel()
    {
        if(Order::all()->count()){
        return \Maatwebsite\Excel\Facades\Excel::download(new \App\Exports\OrdersExport(), 'orders_'.time().'.xlsx');
        }
         else{
            return  redirect()->back()->withErrors('No Order to generate file');
         }
    }
    public function InfoOrder($order_id)
    {


    try {



        $order = Order::query()->find($order_id);
        if(is_null($order)) return $this->sendError(__('custom_messages.general_messages.item_not_found'));



        return view("backend.orders.info",compact("order"));


    } catch (\Exception $ex) {
        return redirect()->back()->withErrors($ex->getMessage());
    }
}



    public function destroyOrderItem($id)
    {
        try {
            $orderItem = OrderItems::query()->find($id);
            if(is_null($orderItem))  return  redirect()->back()->withErrors(__("Deleted Faild."));

            DB::beginTransaction();

            $orderItem->delete();

            DB::commit();
            return  redirect()->back()->withStatus(__('custom_messages.general_messages.crud.deleted'));
        } catch (\Exception $ex) {
            DB::rollBack();
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }
}





