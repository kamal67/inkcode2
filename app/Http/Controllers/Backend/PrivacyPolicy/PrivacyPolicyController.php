<?php

namespace App\Http\Controllers\Backend\PrivacyPolicy;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\PrivacyPolicy\PrivacyPolicy;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PrivacyPolicyController extends BaseController
{

    /**
     * @return Application|Factory|View|RedirectResponse
     */
    public function index()
    {
        try {
            $p_en = PrivacyPolicy::query()->where('lang', 'en')->first();
            $p_ar = PrivacyPolicy::query()->where('lang', 'ar')->first();

            return view('backend.PrivacyPolicy.index', compact('p_en', 'p_ar'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    public function edit()
    {
        try {
            $p_en = PrivacyPolicy::query()->where('lang', 'en')->first();
            $p_ar = PrivacyPolicy::query()->where('lang', 'ar')->first();

            return \view('backend.PrivacyPolicy.edit', compact('p_en', 'p_ar'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    public function update(Request $request)
    {
        try {

            $rules = [
                'description_en' => 'required',
                'description_ar' => 'required',
            ];
            $res_validate = $this->checkRequest($request, $rules);
            if (!$res_validate['status']) return redirect()->back()->withErrors($res_validate['messages']);

            $p_en = PrivacyPolicy::query()->where('lang', 'en')->first();
            $p_ar = PrivacyPolicy::query()->where('lang', 'ar')->first();

            $p_en->description = $request->description_en;
            $p_ar->description = $request->description_ar;

            $p_en->save();
            $p_ar->save();

            return redirect(route('privacyPolicy.index'))->withStatus(__('custom_messages.general_messages.crud.updated'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
