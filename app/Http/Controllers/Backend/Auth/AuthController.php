<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/**
 * Class AuthController
 * @package App\Http\Controllers\Backend\Auth
 */
class AuthController extends BaseController
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function showLoginForm()
    {
        try {
            return view('backend.auth.login');
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login(Request $request)
    {
        try {
            $rules = [
                'password' => 'required',
                'email' => 'email|required'
            ];
            $credentials = $request->only('password', 'email');
            $validator = Validator::make($credentials, $rules);

            if ($validator->fails()) {
                //return $this->sendError($validator->errors(), 400);
                //return redirect('login');
                return redirect()->back()->withErrors($validator->getMessageBag());
            }
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                //return $this->sendToken($token, 200);
                session()->push('showToastr', 'msg');
                return redirect('/');
            } else {
                //return $this->sendError(__('auth.failed'), 401);
                return redirect()->back()->withErrors('The information you’ve entered is incorrect');
            }
        }

        catch (\Exception $ex) {
            dd($ex);
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        try {
            if (Auth::check()) {
                Auth::logout();
            }
            return redirect('login');
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }
}
