<?php

namespace App\Traits;

use App\Models\NotificationLogs;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Response\DownstreamResponse;

trait NotificationTrait
{
    public function sendNotification(array $dataNotification, $notification_id)
    {
        if (count($dataNotification['tokens']) === 0) {
            return false;
        }
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($dataNotification['title']);
        $notificationBuilder->setBody($dataNotification['body'])
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['additional_data' => $dataNotification['additional_data']]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $downstreamResponse = FCM::sendTo($dataNotification['tokens'], $option, $notification, $data);

        return $this->handle_results($downstreamResponse, $notification_id, count($dataNotification['tokens']));
    }

    public function handle_results(DownstreamResponse $downstreamResponse, $notification_id, $num_sent_to)
    {
        if (!$downstreamResponse || is_null($notification_id)) {
            return false;
        }
        $numberSuccess = $downstreamResponse->numberSuccess();
        $numberFailure = $downstreamResponse->numberFailure();
        $numberModification = $downstreamResponse->numberModification();

        // return Array - you must remove all this tokens in your database
        $tokensToDelete = $downstreamResponse->tokensToDelete();

        // return Array (key : oldToken, value : new token - you must change the token in your database)
        $tokensToModify = $downstreamResponse->tokensToModify();

        // return Array - you should try to resend the message to the tokens in the array
        $tokensToRetry = $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:error) - in production you should remove from your database the tokens present in this array
        $tokensWithError = $downstreamResponse->tokensWithError();

        $log = NotificationLogs::create([
            'notification_id' => $notification_id,
            'number_sent_to' => $num_sent_to,
            'number_success' => $numberSuccess,
            'number_failure' => $numberFailure,
            'number_modification' => $numberModification,
            'tokens_to_delete' => json_encode($tokensToDelete),
            'tokens_to_modify' => json_encode($tokensToModify),
            'tokens_to_retry' => json_encode($tokensToRetry),
            'tokens_with_errors' => json_encode($tokensWithError),
        ]);

        return true;
    }

    public function notificationToArray($noti,$fetchTokens=true): array
    {
        $lang = App::getLocale();
        $data = [];

        $data['title'] = $noti['title_'.$lang];
        $data['body'] = $noti['body_'.$lang];
        if ($fetchTokens)$data['tokens'] = User::getTokens($noti->receiver_id);

        return $data;
    }


    public function sendNotificationNewOrder(array $dataNotification, $notification_id = null)
    {
        try{
            if (count($dataNotification['tokens']) === 0) {
                return false;
            }
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);

            $notificationBuilder = new PayloadNotificationBuilder($dataNotification['title']);
            $notificationBuilder->setBody($dataNotification['body'])
                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(['additional_data' => $dataNotification['additional_data'] ??[]]);

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();
            $downstreamResponse = FCM::sendTo($dataNotification['tokens'], $option, $notification, $data);

            return true;
        }
        catch(\Exception $exception){

            Log::error($exception);
            return true;
        }

    }





}
