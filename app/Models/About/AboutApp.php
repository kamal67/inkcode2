<?php

namespace App\Models\About;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutApp extends Model
{
    use HasFactory;

    protected $fillable = [
        'email',
        'app_version',
        'logo',
        'lang',
        'information',
        'is_deleted'];
}
