<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_apps', function (Blueprint $table) {
            $table->id();
            $table->string('email')->nullable();
            $table->string('app_version')->nullable();
            $table->string('logo')->nullable();
            $table->string('lang')->default('en');;
            $table->longText('information')->nullable();
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_apps');
    }
}
